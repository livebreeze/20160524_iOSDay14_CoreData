//
//  ViewController.swift
//  20160524_iOSDay14_CoreData
//
//  Created by ChenSean on 5/24/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit
import CoreData // 記得 import CoreData

class ViewController: UIViewController, UIDocumentPickerDelegate {
    
    @IBOutlet weak var txtUid: UITextField!
    @IBOutlet weak var txtCname: UITextField!
    
    let app = UIApplication.sharedApplication().delegate as! AppDelegate
    var list = [String: Member]()
    func documentPicker(controller: UIDocumentPickerViewController, didPickDocumentAtURL url: NSURL) {
        print(url)
    }
    
    @IBAction func saveButton(sender: AnyObject) {
        
        let user = Member.insertNew(app)
        user.userid = txtUid.text
        user.cname = txtCname.text
        user.tel = txtTel.text
        user.mynumber = 10
        let image = UIImage(named: "index-slider-01")
        user.headImage = UIImagePNGRepresentation (image!)
        
        app.saveContext() // 將值執行寫進 db        
    }
    
    @IBAction func listAllData(sender: AnyObject){
        Member.listAllData(app)
    }
    
    @IBAction func deleteButton(sender: AnyObject) {
        Member.removeAll(app)
    }
    
    @IBOutlet weak var headImage: UIImageView!
    
    @IBAction func a01Indo(sender: AnyObject) {
        
        if let user = list["a01"] {
            headImage.image = UIImage(data: user.headImage!)
        }
    }
    @IBOutlet weak var txtTel: UITextField!

    @IBAction func updateDataButton(sender: AnyObject) {
        let user = list["b01"] // 只要對 list 操作，改完存檔，就會 update entity 的 db 內容了
        user?.userid = "a03"
        app.saveContext()
    }
    
    @IBAction func pickupFileButton(sender: AnyObject) {
        let docPicker = UIDocumentPickerViewController(documentTypes: ["public.jpeg"], inMode: .Import)
        docPicker.delegate = self

        presentViewController(docPicker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print(NSHomeDirectory()) // 每個 app 都有自己的 HomeDirectory，利用 HomeDirectory 來找到 SQLLite 在哪
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

