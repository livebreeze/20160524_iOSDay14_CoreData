//
//  Member.swift
//  20160524_iOSDay14_CoreData
//
//  Created by ChenSean on 5/26/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import Foundation
import CoreData


class Member: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    class func insertNew(app: AppDelegate) -> Member {
        return NSEntityDescription.insertNewObjectForEntityForName("Member", inManagedObjectContext: app.managedObjectContext) as! Member
    }
    
    class func removeAll(app: AppDelegate) {
        let fetch = NSFetchRequest(entityName: "Member")
        if #available(iOS 9.0, *){
            let batch = NSBatchDeleteRequest(fetchRequest: fetch)
            try! app.persistentStoreCoordinator.executeRequest(batch, withContext: app.managedObjectContext)
        } else {
            do {
                let allUsers = try app.managedObjectContext.executeFetchRequest(fetch) as! [NSManagedObject]
                for user in allUsers {
                    app.managedObjectContext.deleteObject(user)
                }
                app.saveContext()
            } catch {
                
            }
        }
    }
    
    class func listAllData(app: AppDelegate) {
        let fetch = NSFetchRequest(entityName: "Member")
        let predicate = NSPredicate(format: "uid like 'a*'", argumentArray: nil)
        fetch.predicate = predicate
        let sort = NSSortDescriptor(key: "uid", ascending: false)
        
        fetch.predicate = predicate
        fetch.sortDescriptors = [sort] // 可以對很多欄位同時做排序
        
        do {
            let allUsers = try app.managedObjectContext.executeFetchRequest(fetch) as! [Member]
            for user in allUsers {
                print("user id = \(user.userid)")
                print("user cname = \(user.cname)")
                print("----------------")
            }
        }
        catch {
            
        }
    }

}
