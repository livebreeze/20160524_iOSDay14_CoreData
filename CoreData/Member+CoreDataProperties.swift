//
//  Member+CoreDataProperties.swift
//  20160524_iOSDay14_CoreData
//
//  Created by ChenSean on 5/26/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Member {

    @NSManaged var cname: String?
    @NSManaged var headImage: NSData?
    @NSManaged var mynumber: NSNumber?
    @NSManaged var userid: String?
    @NSManaged var tel: String?
    @NSManaged var own: Member?

}
